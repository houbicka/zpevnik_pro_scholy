# import packages
import requests
import json
import re

# build query to use for calling API
query = """query
{song_lyrics
    {song_number, name, lyrics_no_chords}
}
"""

# create template for output json
output_json = {"songbook": "Zpěvník pro scholy", "songs": []}

# get data from zpevnik pro scholy API
url = 'https://zpevnik.proscholy.cz/graphql'
r = requests.get(url, json={'query': query})
json_data = json.loads(r.text)

# take lyrics 
songs = json_data["data"]["song_lyrics"]

# go through each song in all lyrics
for song in songs:
    song_number = str(song["song_number"]) # take number of each song as string
    song_name = song["name"] # take name of each song
    song_json = {"id":song_number, "name":song_name, "verses":[]} # create template for song json

    # set delimiters
    if "\r\n" in song["lyrics_no_chords"]: #decide which strophes delimiter is used
        delimiter = "\r\n" #linux style
    else: 
        delimiter = "\n" #windows style

    # strophes delimiter can have a space in between or not, both is accepted
    different_delimiters = delimiter+delimiter + "|" + delimiter+' '+delimiter
    # split the song into strophes
    strophes = re.split(different_delimiters, song["lyrics_no_chords"])
       
    # initialize variables for chorus
    chorus = []
    check_next_for_chorus = 0

    # go through each strophe and split it into verses
    for i in strophes:
        strophe_output = []
        # split the strophe into verses
        verses = i.split(delimiter)

        # check the verse if it is the beginning of chorus
        for j in verses: # go through each line
            # blank chorus
            R = (j == "R:" or j =="(R:)" or j == "R: " or j == "R.")
            if R:
                break

            # append the line to the strophe
            strophe_output.append(j.strip())
        
        if not R:
        # get additional strophes of chorus
            if check_next_for_chorus == 1:
                # the next strophe includes number (the chorus is complete)
                is_strophe_with_number = bool(re.search('\d+.', strophe_output[0]))

                if not is_strophe_with_number:
                    chorus.append(strophe_output) # add another strophe to the chorus
                else:
                    check_next_for_chorus = 0 # the next strophe won't be checked
        
        # set chorus variable for other occurences
            if "R:" in strophe_output[0] and len(strophe_output[0]) > 6:
                # add the strophe to chorus
                chorus.append(strophe_output)
                # the chorus can be longer, check the next strophe
                check_next_for_chorus = 1
        # add strophe to the song
            song_json["verses"].append(strophe_output)
        
        # the R: has to be replaced with the chorus, add the strophes one by one
        else:
            for i in chorus:
                song_json["verses"].append(i)
        
    # add the song to the output json
    output_json["songs"].append(song_json)

# generate the output into the path
with open('OutputZpevnikProScholy.json', 'w', encoding='utf8') as fp:
    json.dump(output_json, fp, indent = 2, ensure_ascii=False)



