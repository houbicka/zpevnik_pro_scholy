# Zpevnik_pro_scholy
## Source
There is a public API which provides data in json. Info was provided by Mira Šerý jira@proscholy.atlassian.net.
Use the following address to get the song number and name and lyrics without chords: 
https://zpevnik.proscholy.cz/graphql?query=query{song_lyrics{song_number, name, lyrics_no_chords}} 

### Documentation
Documentation available on https://zpevnik.proscholy.cz/graphql-playground (see the Docs tab on the right to choose the desired query). For example, a query to get external links and their authors for the song with ID 100. Use the panel on the left to show the output on the right.
```
query {
  song_lyric (id: 100) {
    id
    name
    externals {
      authors {
        name
      }
      url
    }
  }
}
```

## Python script
### Used packages
The package *requests* has to be installed (`pip install requests`) so that it is possible to get data by calling API. 
The python *re* package was used for working with regex expressions.
The last package to be imported is *json* for creating the json output.
### Calling API
API is called based on information mentioned in the first paragraph and using the *requests* package.
### Getting data
Song number and name are got from the source json. Lyrics are divided into strophes using different delimiters. Chorus signs (of different formats) are replaced by the actual text.
### Generating output
The result of the cycle for each song is exported into the output file *OutputZpevnikProScholy.json*. 